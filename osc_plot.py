import webbrowser
from time import sleep
from subprocess import run, Popen
from osc_to_teleplot import OscToTeleplot
import pathlib
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("osc_port", type=int,
                    help="Port of the osc listener")
arguments = parser.parse_args()


this_scripts_directory = pathlib.Path(__file__).parent.resolve()

teleplot_server_path = this_scripts_directory / "teleplot" / "server"
teleplot_server_main_path = teleplot_server_path / "main.js"

# if we don't have teleplot, fetch it from git
if not (this_scripts_directory / "teleplot").is_dir():
    run(["git", "clone", "https://github.com/nesnes/teleplot"])
    run(["npm", "i"], cwd=str(teleplot_server_path))

# start teleplot
Popen(["node", str(teleplot_server_main_path)])
# wait for teleplot to start. 0.5 seconds is generous, teleplot is pretty fast to start.
sleep(0.5)

teleplot_default_port = 47269
teleplot_host = "127.0.0.1"

# open teleplot in a new tab of your default web browser
webbrowser.open_new_tab("127.0.0.1:8080")

# start the osc listening server.
osc_to_teleplot = OscToTeleplot(arguments.osc_port, teleplot_host, teleplot_default_port)
osc_to_teleplot.start()
