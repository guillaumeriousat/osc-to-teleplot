import argparse
import json
import sys
import threading
import time
import re
from dataclasses import dataclass, field
from pathlib import Path
from signal import SIGINT, signal
from typing import Any
from pythonosc import udp_client
# Thanks Rochana (https://gitlab.com/rfardon) from which I took the base structure of this file and the neat progress bar.


# application threads stop signal
task_stop_signal = threading.Event()

# OSC client global variables
address = "127.0.0.1"
port = 9000

list_index_pattern = r"\[\d+\]"
array_message_pattern = ".+" + list_index_pattern


@dataclass
class OSCMessage:
    address: str
    timestamp: float
    value: Any

def get_messages_from_file(teleplot_file_path):
    osc_messages = []
    with open(teleplot_file_path, "r") as teleplot_file:
        teleplot_data = json.load(teleplot_file)
        telemetries = teleplot_data["telemetries"]
        # These keys are for messages that have non list values
        non_array_valued_addresses = filter(lambda key: not re.match(array_message_pattern, key), telemetries.keys())
        for address in non_array_valued_addresses:
            # consolidate pendingData and data
            timestamps = telemetries[address]["data"][0] + telemetries[address]["pendingData"][0]
            values = telemetries[address]["data"][1] + telemetries[address]["pendingData"][1]
            for i in range(len(values)):
                osc_messages.append(OSCMessage(address=address, timestamp=timestamps[i], value=values[i]))

        # This is the logic for array valued osc addresses
        # This is a bit complicated : the osc_to_teleplot script splits array valued messages into different addresses :
        # if you send /values [10,20,30], teleplot will receive it as /values[0] 10 /values[1] 20 /values[2] 30 with an identical timestamp for each value.
        # To send back the right things, we need to recompose the arrays and send them as one OSC messages.
        # What makes this delicate is that we have no guarantee that /values has only 3 array element from moment to moment. We need to dynamically
        # find how many element the array had for every timestamp that matches \/value\[\d+\].

        array_valued_addresses = list(filter(lambda key: re.match(array_message_pattern, key), telemetries.keys()))
        array_valued_unique_addresses = {re.sub(list_index_pattern, "", address) for address in array_valued_addresses}
        for address_prefix in array_valued_unique_addresses:
            # get list index from addresses
            def sort_func(address):
                return int(re.search(list_index_pattern, address).group(0).replace("[", "").replace("]",""))
            # all the addresses that matches the prefix sorted by list index
            all_addresses = sorted([address for address in array_valued_addresses if address_prefix in address], key=sort_func)
            # all the unique timestamps at which teleplot received something matching this address prefix.
            consolidated_timestamps = set()
            # the consolidated timestamps are the set intersection of all the timestamps for every address that matches our prefix
            for address in all_addresses:
                consolidated_timestamps |= set(telemetries[address]["data"][0] + telemetries[address]["pendingData"][0])
            # now that we have our consolidated timestamps, we can iterate over every one of them and reconstruct the
            # value array we need to send for every one of them. We should always have at least two values per timestamp.
            for timestamp in consolidated_timestamps:
                values_array = []
                # for every address, in list index order, check if the address has a value for the current timestamp. if it does, append the values to
                # the values array.
                for address in all_addresses:
                    timestamps = telemetries[address]["data"][0] + telemetries[address]["pendingData"][0]
                    values = telemetries[address]["data"][1] + telemetries[address]["pendingData"][1]
                    if timestamp in timestamps:
                        # append the value corresponding to the timestamp.
                        values_array.append(values[timestamps.index(timestamp)])
                osc_messages.append(OSCMessage(address=address_prefix, timestamp=timestamp, value=values_array))
    # sort the osc messages (by timestamp as stated in their dataclass)
    return sorted(osc_messages, key=lambda message: message.timestamp)

def busy_wait(start_time, interval):
    while True:
        if (start_time + interval) <= (current_time := time.time()):
            return current_time
            break

def send_data(osc_messages, loop, wait_before_loop):
    start_time = time.time()
    done_once = False
    intervals_sum = 0
    while(loop or not done_once):
        for idx, osc_message in enumerate(osc_messages):
            # this is nonsensical for the first message but we skip waiting for the first message anyways.
            timeout = osc_message.timestamp - osc_messages[idx - 1].timestamp
            if idx != 0:
                intervals_sum += timeout
            current_time = 0
            start_time = busy_wait(start_time, timeout if idx != 0 else 0)
            update_progress_bar(idx, osc_messages)
            client.send_message(osc_messages[idx].address, osc_messages[idx].value)

        done_once = True
        post_loop_interval = float(wait_before_loop) if wait_before_loop != "auto" else intervals_sum/len(osc_messages)
        start_time = busy_wait(start_time, post_loop_interval)
    stop()


def stop():
    """Register a stop signal informing all thread tasks to stop their work"""
    global task_stop_signal
    task_stop_signal.set()
    print("\n\nExiting...")


def update_progress_bar(progress, osc_messages):
    length = 40
    maximum = len(osc_messages)
    filled_length = int(round(length * progress / float(maximum)))

    percent = round(100.0 * progress / float(maximum), 1)
    bar = " " * filled_length + "-" * (length - filled_length)

    sys.stdout.write("\r")
    sys.stdout.write(
        "%s [%s] %s%s ...%s\r" % ("Progress", bar, percent, "%", "Complete")
    )
    sys.stdout.flush()


def handler(signal_received, frame):
    """Signal handler to stop when receiving SIGINT"""
    sys.exit(0)


signal(SIGINT, handler)

parser = argparse.ArgumentParser()

parser.add_argument(
    "path",
    type=str,
    help="Path of the teleplot file",
)
parser.add_argument(
    "-a", "--address",
    default="127.0.0.1",
    help="The IP or hostname address to send OSC to",
)
parser.add_argument(
    "-p", "--port", type=int, default=9000, help="The port to send OSC to"
)

parser.add_argument(
    "-i", "--wait-before-loop", type=str, default="auto", help="The time this waits before looping. If set to 'auto' value, this will wait the average interval found in the data. Otherwise input an amount of seconds in float."
)

parser.add_argument(
    "-nl", "--no-loop", action="store_true", help="Use this flag if you don't want data to be read in a loop"
)


args = parser.parse_args()

address = args.address
port = args.port

client = udp_client.SimpleUDPClient(address, port)

path = args.path

osc_messages = get_messages_from_file(path)


send_data_thread = threading.Thread(target=lambda: send_data(osc_messages, not args.no_loop, args.wait_before_loop))
send_data_thread.start()
