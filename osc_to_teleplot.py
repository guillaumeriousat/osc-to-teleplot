import socket
from threading import Thread
from pythonosc import osc_server, udp_client
from pythonosc.dispatcher import Dispatcher
from time import time

class OscToTeleplot:
    def __init__(self, osc_port, teleplot_host, teleplot_port):

        def receive_osc(addr, *args):
            self.send_to_teleplot(addr, *args)

        self.teleplot_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.teleplot_host = teleplot_host
        self.teleplot_port = teleplot_port
        self.dispatcher = Dispatcher()
        self.dispatcher.map("*", receive_osc)

        self.server = osc_server.ThreadingOSCUDPServer(
            ("0.0.0.0", osc_port), self.dispatcher)

        self.server.serve_forever()

    def start(self):
        """this starts the osc server and blocks forever"""
        self.server.serve_forever()

    def start_threaded(self):
        self.serve_thread = Thread(target=lambda:self.start())

    def _send_to_socket(self, label, value, timestamp):
        """sends a single value """
        cleaned_label = label.replace(':', '_')
        print(label, value)
        self.teleplot_socket.sendto(f"{cleaned_label}:{timestamp}:{value}".encode(), (self.teleplot_host, self.teleplot_port))
    def send_to_teleplot(self, label, *values):
        # teleplot wants a timestamp in ms. We can let it calculate it but it tends to bunch messages together if
        # we do that.
        timestamp_milis = int(time()*1000)
        if len(values) == 1:
            self._send_to_socket(label, values[0], timestamp_milis)
        else:
            for i, val in enumerate(values):
                self._send_to_socket(label+f"[{i}]", val, timestamp_milis)


if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--osc-port", type=int,
                        help="Port of the osc listener", required=True)
    parser.add_argument("-o", "--teleplot-port", type=int,
                        help="Port of the osc listener", required=True)
    parser.add_argument("-w", "--teleplot-host", type=str, default="teleplot.fr",
                        help="Port of the osc listener")

    arguments = parser.parse_args()

    osc_to_teleplot = OscToTeleplot(arguments.osc_port, arguments.teleplot_host, arguments.teleplot_port)
    osc_to_teleplot.start()
