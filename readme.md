# OSC to teleplot

This repository offers two scripts that makes it easy plot OSC messages using [teleplot](https://teleplot.fr/).

Using teleplot's export function, you can also use `teleplot_to_osc.py` to replay messages captured by teleplot !

## osc_to_teleplot.py

This method is the most flexible as it permits you to plot any port on any teleplot host. However, you will need to furnish your own teleplot host and to type more.

### requirements
`sudo pip install -r requirements.txt`

### usage

You can the use the script in this way : `python osc_to_teleplot.py --osc-port <your osc listening port> --teleplot-port <the teleplot port> --teleplot-host <host of the teleplot you whish to send to>`

If you want to use teleplot.fr, you're in luck, its the default host. Go to teleplot.fr, take note of the port it gives you and do

`python osc_to_teleplot.py --osc-port <your osc listening port> --teleplot-port <the teleplot port you took note of>`

## osc_plot.py

This method has more requirements but is lazyer.

### requirements

- Have `node` installed
- Have `git` installed
- Have `npm` installed

`sudo pip install -r requirements.txt`

### usage

`python osc_plot.py <your osc listening port>`

This will fetch teleplot from git for you (if its the first time you run it), set it up and set the correct host and port for you.

This will even start your favourite web browser and open teleplot on it.

## teleplot_to_osc.py

This allows you to take teleplot's json export and to play it back as osc messages. This script assumes that you are trying to playback messages that has been sent to teleplot from
one of the two scripts above.

### requirements
`sudo pip install -r requirements.txt`

### usage

To record data:
- Start `osc_plot.py` or `osc_to_teleplot.py`
- In teleplot, switch the window size to no window
- Start sending osc data the recording process
- When you have the data you are interested in, press the `export json` button in teleplot
- Rename the file to something meaningful and move it out of the Download folder

To play it back :

`python teleplot_to_osc.py <path of the json file> -p <port you want to send to>`
